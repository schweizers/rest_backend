<!DOCTYPE html>
<html>
    <head>
        <title>Rest Backend</title>

        <link href="https://fonts.googleapis.com/css?family=Amatic+SC" rel="stylesheet">
        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Amatic SC', cursive;
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
                font-weight: bold;
            }
            hr{
                width: 35%;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">Believe you can and you're halfway there! <hr> Theodore Roosevelt</div>
            </div>
        </div>
    </body>
</html>
