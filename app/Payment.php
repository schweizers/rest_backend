<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    public $table = "payment";

    protected $fillable = [
        'message',
        'amount',
        'from',
        'to',
    ];


    public function account(){
        return $this->belongsTo('App\Account');
    }
}
