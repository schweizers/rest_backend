<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $fillable = [
        'name', 'user_id', 'id_number', 'creditBalance'
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public $table = "account";

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function payment(){
        return $this->hasMany('App\Payment', 'from');
    }
}
