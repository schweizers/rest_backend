<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/', function () {
    return view('welcome');
});


Route::group(['namespace' => 'Rest', 'prefix' => 'rest'], function() {

    Route::post('/login', 'RestController@login');

    Route::post('/register', 'RestController@register');

    Route::post('/account/create', 'RestController@accountCreate');

    Route::get('/account/overview', 'RestController@accountOverview');

    Route::delete('/account/delete/{id}', 'RestController@accountDelete');

    Route::get('/account/others', 'RestController@accountOthers');

    Route::post('/payment', 'RestController@payment');

    Route::get('/logout', 'RestController@logout');





});

