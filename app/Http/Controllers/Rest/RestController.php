<?php

namespace App\Http\Controllers\Rest;

use App\Account;
use App\Http\Controllers\Controller;
use App\Payment;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;


class RestController extends Controller
{
    public function __construct()
    {
        $this->middleware(('jwt.auth'), ['except' => ['login', 'register']]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        // grab credentials from the request

        $credentials = $request->only('email', 'password');
        try {
            // attempt to verify the credentials and create a token for the user
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        $user = $this->userLoad(JWTAuth::toUser($token));

        return response()->json(compact('token', 'user'));


    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {

        try {
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password)
            ]);
        } catch (QueryException $e) {
            return response()->json(['error' => 'User already exists.'], 500);
        }

        $token = JWTAuth::fromUser($user);
        $user = $this->userLoad($user);

        return response()->json(compact('token', 'user'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function accountCreate(Request $request)
    {
        $user = $this->getUser();

        try {
            Account::create([
                    'name' => $request->name,
                    'user_id' => $user->id,
                    'id_number' => strtoupper(str_random(10)),
                    'creditBalance' => 500.00
                ]
            );
        } catch (QueryException $e) {
            return response()->json(['error' => 'An unknown error occured.'], 500);
        }
        $user = $this->userLoad($user);
        return response()->json(compact('user'));

    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function accountOverview()
    {
        $user = $this->getUser();
        $account = Account::where('user_id', $user->id)->get();
        $account->load('user');
        return response()->json(compact('account'));

    }

    public function accountDelete($id)
    {
        $account = Account::find($id);
        if ($account->user_id == $this->getUser()->id) {
            $account->delete();
            return response()->json(['msg' => 'successfully deleted']);

        } else {
            return response()->json(['error' => 'invalid_credentials'], 401);

        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function accountOthers()
    {
        $account = Account::all();
        $account->load('user');
        return response()->json(compact('account'));

    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function payment(Request $request)
    {
        try {
            $user = $this->getUser();

            $from = $user->account()->where('id_number', $request->from)->first();
            $to = Account::where('id_number', $request->to)->first();
            $amount = (float)$request->amount;

            if ($amount > 0.00 && $from->creditBalance > $amount) {
                $from->creditBalance = ((float)$from->creditBalance - $amount);
                $to->creditBalance = ((float)$to->creditBalance + $amount);

                Payment::create([
                    'message' => $request->message,
                    'amount' => $request->amount,
                    'from' => $from->id,
                    'to' => $to->id
                ]);
                $from->save();
                $to->save();
                return response()->json(compact('from','to'));

            } else {
                return response()->json(['error' => 'not enough credit']);

            }
        } catch (\Exception $e) {
            return response()->json(['error' => ' seems like an error occured']);

        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        try {
            JWTAuth::invalidate(JWTAuth::getToken());
            return response()->json(['msg' => 'successfully logged out']);
        } catch (\Exception $e) {
            return response()->json(['error' => 'seems like an error occured']);
        }

    }

    /**
     * @param User $user
     * @return User
     */
    public function userLoad(User $user)
    {
        $user->load('account.payment');
        return $user;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return JWTAuth::parseToken()->authenticate();
    }

}
