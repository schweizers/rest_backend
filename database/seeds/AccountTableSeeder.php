<?php

use Illuminate\Database\Seeder;

class AccountTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('account')->insert([
            'name' => 'admin_account',
            'user_id' => 1,
            'id_number' => strtoupper(str_random(10)),
            'creditBalance' => 500.00,
            'created_at' => \Carbon\Carbon::now(),
        ]);
        DB::table('account')->insert([
            'name' => 'admin_account2',
            'user_id' => 1,
            'id_number' => strtoupper(str_random(10)),
            'creditBalance' => 500.00,
            'created_at' => \Carbon\Carbon::now(),
        ]);
        DB::table('account')->insert([
            'name' => 'root_account',
            'user_id' => 2,
            'id_number' => strtoupper(str_random(10)),
            'creditBalance' => 500.00,
            'created_at' => \Carbon\Carbon::now(),
        ]);
        DB::table('account')->insert([
            'name' => 'root_account',
            'user_id' => 2,
            'id_number' => strtoupper(str_random(10)),
            'creditBalance' => 500.00,
            'created_at' => \Carbon\Carbon::now(),
        ]);
    }
}
