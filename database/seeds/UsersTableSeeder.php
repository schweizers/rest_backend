<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('secret'),
            'created_at' => \Carbon\Carbon::now(),
        ]);
        DB::table('users')->insert([
            'name' => 'root',
            'email' => 'root@root.com',
            'password' => bcrypt('secret'),
            'created_at' => \Carbon\Carbon::now(),
        ]);
    }
}
