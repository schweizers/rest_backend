<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('message');
            $table->decimal('amount', 9, 2);
            $table->integer('from')->unsigned();
            $table->integer('to')->unsigned();
            $table->timestamps();

            $table->foreign('from')->references('id')->on('account')->onDelete('cascade');
            $table->foreign('to')->references('id')->on('account');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payment');
    }
}
